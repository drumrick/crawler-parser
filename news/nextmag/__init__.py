import requests
import re
from datetime import datetime
from bs4 import BeautifulSoup

def crawl_list(url, driver=None):
    return requests.get(url).text


def parse_list(html):
    data = {
        'next_page': None,
        'latest_post_date': None,
        'links': []
    }

    links = []
    datetimes = []

    domain_prefix = 'https://www.nextmag.com.tw'
    soup = BeautifulSoup(html, 'html5lib')
    for element in soup.find_all('a', class_='fill-link'):
        time_ = element.find('time')
        if not time_:
            continue

        link = domain_prefix + element.get('href')
        links.append({
            'url': link,
            'metadata': {}
        })

        dt = datetime.strptime(time_.text, '%Y年%m月%d日 %H:%M')
        datetimes.append(dt)

    data['links'] = links
    data['latest_post_date'] = max(datetimes).strftime('%Y-%m-%dT%H:%M:%S+08:00')
    return data


def crawl_detail(url, driver=None):
    return requests.get(url).content


def parse_detail(html):

    def parse_datetime(soup):
        time_ = soup.find('time')
        dt = datetime.strptime(time_.text, '%Y年%m月%d日')
        return dt.strftime('%Y-%m-%dT%H:%M:%S+08:00')

    def parse_metadata(soup):
        return {
            'board': soup.find('div', class_='category').text
        }

    def parse_title(soup):
        board = soup.find('div', class_='category')
        title = board.find_next_siblings()[0]
        return title.text.strip()

    def parse_content(soup):
        article = soup.find('div', class_='article-content')
        paragraph = []
        for p in article.find_all('p'):
            text = p.text.strip()
            if len(text) == 0:
                continue
            paragraph.append(text)

        content = '\n'.join(paragraph)
        return content.split('更多壹週刊')[0]


    soup = BeautifulSoup(html, 'html5lib')
    data = {
        'datetime': parse_datetime(soup),
        'author': None,
        'title': parse_title(soup),
        'content': parse_content(soup),
        'metadata': parse_metadata(soup),
        'comments': [],
    }

    regex = re.search('撰文：([^）　)]+)', data['content'])
    if regex:
        authors = regex.groups()[0].split('、')
        data['author'] = authors[0]
        if len(authors) > 0:
            data['metadata']['authors'] = authors
    return data


if __name__ == '__main__':
    # testing crawl_list/parse_list
    for url in [
        'https://www.nextmag.com.tw/breakingnews/mosthit',
        'https://www.nextmag.com.tw/breakingnews/1clock',
        'https://www.nextmag.com.tw/breakingnews/life'
    ]:
        html = crawl_list(url)
        list_data = parse_list(html)

        # testing crawl_detail/parse_detail
        html = crawl_detail(list_data['links'][0]['url'])
        detail_data = parse_detail(html)
        print(detail_data)
        print('=' * 20)

    '''
    # testing crawl_detail/parse_detail
    for url in [
        'https://www.nextmag.com.tw/realtimenews/news/470606',
        'https://www.nextmag.com.tw/realtimenews/news/470990'
    ]:
        html = crawl_detail(url)
        data = parse_detail(html)
        print(data)
    '''
